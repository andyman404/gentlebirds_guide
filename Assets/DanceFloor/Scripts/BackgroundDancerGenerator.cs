﻿using UnityEngine;
using System.Collections;

public class BackgroundDancerGenerator : MonoBehaviour {

    public Transform dancerPrefab;

    public int minGroupSize = 1;
    public int maxGroupSize = 4;

    public int groups = 8;
    public float range = 10.0f;
    public float memberRange = 2.0f;
	// Use this for initialization
	void Start () {
        for (int i = 0; i < groups; i++)
        {
            int groupSize = Random.Range(minGroupSize, maxGroupSize + 1);

            // determine the group's position
            Vector3 groupPosition = new Vector3(Random.Range(-range, range), 0.0f, Random.Range(-range, range));

            // add the members to the group
            for (int j = 0; j < groupSize; j++)
            {
                // member is a random distance from the group center
                Vector3 myPosition = groupPosition + new Vector3(Random.Range(-memberRange, memberRange), 0.0f, Random.Range(-memberRange, memberRange)); 

                Transform instance = Instantiate(dancerPrefab, myPosition, Quaternion.LookRotation((groupPosition-myPosition).normalized)) as Transform;
                instance.SetParent(transform);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {

	}
}
