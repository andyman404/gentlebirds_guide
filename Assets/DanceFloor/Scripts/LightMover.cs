﻿using UnityEngine;
using System.Collections;

public class LightMover : MonoBehaviour {

    public float minInterval = 6.0f;
    public float maxInterval = 12.0f;
    public float range = 20.0f;
    private Vector3 originalPosition;

    private Vector3 startPosition;
    private Vector3 targetPosition;

    private float startTime = 0.0f;
   
    private float endTime = 0.0f;

	// Use this for initialization
	void Start () {
        originalPosition = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time > endTime)
        {
            startPosition = transform.localPosition;
            targetPosition = new Vector3(Random.Range(-range, range),
                Random.Range(0.0f, 5.0f),
                Random.Range(-range, range)) + originalPosition;
            startTime = Time.time;
            endTime = startTime + Random.Range(minInterval, maxInterval);
        }

        transform.localPosition = Vector3.Lerp(startPosition, targetPosition, Mathf.InverseLerp(startTime, endTime, Time.time));
	}
}
