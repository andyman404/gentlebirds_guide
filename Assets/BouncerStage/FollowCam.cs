﻿using UnityEngine;
using System.Collections;

public class FollowCam : MonoBehaviour {

	public Transform target;
	public Vector3 focusOffset;
	public Vector3 zoomInOffset;
	public Vector3 zoomOutOffset;

	public float minSpeed = 0.0f;
	public float maxSpeed = 10.0f;
	
	public float zoom = 1.0f;
	public float zoomLerpRate = 0.5f;
	public float yLerpRate = 1.0f;

	private Vector3 lastPosition;
	// Use this for initialization
	void Start () {
		lastPosition = target.position;
	}
	
	void FixedUpdate () {


		// find the position of the target
		Vector3 newPosition = target.position;

		// find the speed of the target based on the last position of it
		float speed = Vector3.Distance (newPosition, lastPosition) / Time.deltaTime;

		// adjust the zoom based on the speed
		zoom = Mathf.Lerp (zoom, Mathf.InverseLerp (minSpeed, maxSpeed, speed), zoomLerpRate * Time.deltaTime);

		// old position of the camera
		Vector3 oldPosition = transform.position;

		// new position of the camera
		newPosition = target.position + Vector3.Lerp (zoomInOffset, zoomOutOffset, zoom);

		newPosition.y = Mathf.Lerp (oldPosition.y, newPosition.y, Time.deltaTime * yLerpRate);
		transform.position = newPosition;


		lastPosition = target.position;
	}
}
