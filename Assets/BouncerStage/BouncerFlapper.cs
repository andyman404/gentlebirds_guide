﻿using UnityEngine;
using System.Collections;

public class BouncerFlapper : MonoBehaviour {
    Animator anim;

    public float nextTime = 0.0f;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
    private float leftValue = 1.0f;
    private float rightValue = 1.0f;
	// Update is called once per frame
	void Update () {
        if (Time.time > nextTime)
        {
            leftValue = Mathf.Round(Random.value + 0.2f);
            rightValue = Mathf.Round(Random.value + 0.2f);
            nextTime = Time.time + Random.Range(3.0f, 5.0f);

        }
        anim.SetFloat("wing.l.x", leftValue, 0.5f, Time.deltaTime);
        anim.SetFloat("wing.r.x", rightValue, 0.5f, Time.deltaTime);
	}
}
