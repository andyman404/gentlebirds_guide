﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.ComponentModel;

public class BirdAppearancePicker : MonoBehaviour {

	public Gradient hairColors;
	public Gradient featherColors;
	public BirdAppearance bird;
	public Slider hairSlider;
	public Slider featherSlider;
	public Slider lengthSlider;
	public Slider lrSlider;

	public float hairRate = 1.0f;

	// Use this for initialization
	void Start () {
        lengthSlider.normalizedValue = bird.hairLength;
        lrSlider.normalizedValue = bird.hairOption;
	}
	
	// Update is called once per frame
	void Update () {
		bird.hairColor = hairColors.Evaluate (hairSlider.normalizedValue);
		bird.featherColor = featherColors.Evaluate (featherSlider.normalizedValue);
		bird.hairLength = lengthSlider.normalizedValue;
		bird.hairOption = lrSlider.normalizedValue;
	}
}
