﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauserMenu : MonoBehaviour {

    public bool paused = false;
    public static PauserMenu instance = null;

    public GameObject pausePanel;

    void Awake() {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Cancel"))
        {
            if (paused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
	}

    public void Pause()
    {
        paused = true;
        Time.timeScale = 0.0f;
        pausePanel.gameObject.SetActive(true);
    }

    public void Resume()
    {
        paused = false;
        Time.timeScale = 1.0f;
        pausePanel.gameObject.SetActive(false);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void LoadLevel(int level)
    {
        SceneManager.LoadScene(level);
    }
}
