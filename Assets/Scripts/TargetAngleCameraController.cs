﻿using UnityEngine;
using System.Collections;

public class TargetAngleCameraController : MonoBehaviour {

    public GameObject[] targets = new GameObject[2]; //move smoothly
    public Vector3 offset;

    private GameObject target;
    private GameObject prev_target; //perhaps to swtich between different characters

    public float duration;
    private float startTime;

    void Start() {

        //get first target
        target = targets[0];
           
        //TODO make it rotate to the target's rotation as well

        //set optional offset
        if ( offset == Vector3.zero ) { offset = new Vector3(0,0,-5); }

    }

    void LateUpdate() {
        
        //on screen press change target
        if( Input.GetMouseButtonDown(0) ) {
            //prev_target = targets[0];
            target = targets[1];
        }

        //startTime = Time.time - 10;

        float t = (Time.time - startTime) / duration;

        //tweening from one value to the other
        transform.position = new Vector3(Mathf.SmoothStep(0, target.transform.position.x + offset.x, t),
                                         Mathf.SmoothStep(0, target.transform.position.y + offset.y, t),
                                         Mathf.SmoothStep(0, target.transform.position.z + offset.z, t));

        /* //not as smooth
        transform.position = new Vector3(Mathf.Lerp(0, target.transform.position.x + offset.x, Time.time),
                                         Mathf.Lerp(0, target.transform.position.y + offset.y, Time.time),
                                         Mathf.Lerp(0, target.transform.position.z + offset.z, Time.time));
                                         */
        
        //transform.position = target.transform.position + offset;
        transform.LookAt(target.transform);


    }


}

