﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DanceArrowController : MonoBehaviour {

    //things that can be set outside in unity
    public RectTransform ArrowPrefab;
    public float speed;
    public float arrowTimer;
    public float arrowInterval;
    public float arrowScale;
    public float winThresh;
    public float winBonus;
    public float losePenalty;
    public float arrowSpread;
    public float arrowChance;

	public Slider slider;
	public Transform randomBird;

    public ParticleSystem heartParticleSystem;
    public int hitEmitCount = 10;

    public float cheatAmount = 0.01f;

    //arrow stuff
    private RectTransform[] arrows;
    private const int MAX_ARROWS = 20;
    private int current_arrow;
    private float ARROW_X_SPREAD;
    private const float TOP_SCREEN = 500.0f;
    private const float BOT_SCREEN = -500.0f;
    private const float WIN_SPOT = -240.0f;
	private int x_goal = 0, y_goal = 0;

    //target arrows
    private RectTransform upArrow;
    private RectTransform downArrow;
    private RectTransform rightArrow;
    private RectTransform leftArrow;

    private UIFader upFader;
    private UIFader downFader;
    private UIFader leftFader;
    private UIFader rightFader;

    Animator partnerAnim;

	private float moveTimer;

	private const float MOVE_TIME = 18.0f/60.0f;
	private const float MOVE_SPEED = 0.08f;


	// Use this for initialization
	void Start () {
	    
        ARROW_X_SPREAD = arrowScale*100.0f*arrowSpread; 
        arrowTimer = 2;
        current_arrow = 0;
        arrows = new RectTransform[MAX_ARROWS];

        placeTemplateArrows();

        //create arrows
        for(int i=0; i<MAX_ARROWS; i++) {
            arrows[i] = Instantiate( ArrowPrefab ) as RectTransform;
            arrows[i].SetParent( this.transform );
            arrows[i].localPosition = new Vector3(500 - i*100f, -500f);
            arrows[i].localRotation = Quaternion.EulerRotation(0,0,(Mathf.PI/2)*Random.Range(0,3));
            arrows[i].localScale = Vector3.one*arrowScale;   
			arrows[i].gameObject.SetActive(false);
        }
        partnerAnim = randomBird.gameObject.GetComponent<Animator> ();
        partnerAnim.SetBool("dancing", true);
	}
	
	// Update is called once per frame
	void Update () {
        


        //arrow movement
        for(int i=0; i<MAX_ARROWS; i++) {

            //move down
            arrows[i].localPosition +=  Vector3.down * Time.deltaTime * speed;

            //set invis
            if(arrows[i].localPosition.y < BOT_SCREEN) {
				if (arrows [i].gameObject.activeSelf) {
					slider.value += 0.25f;
				}
                arrows[i].gameObject.SetActive(false);
            }

			if( Mathf.Abs( arrows[i].localPosition.y - WIN_SPOT ) <= 80 ) {
				moveTimer = MOVE_TIME;
				if (arrows [i].localRotation == Quaternion.EulerRotation (0, 0, (Mathf.PI / 2) * 0)) {  //up
					x_goal = 0;
					y_goal = 1;
				}
				if (arrows [i].localRotation == Quaternion.EulerRotation (0, 0, (Mathf.PI / 2) * 2)) {   //down
					x_goal = 0;
					y_goal = -1;
				}
				if (arrows [i].localRotation == Quaternion.EulerRotation (0, 0, (Mathf.PI / 2) * 1)) {   //right
					x_goal = 1;
					y_goal = 0;
				}
				if (arrows [i].localRotation == Quaternion.EulerRotation (0, 0, (Mathf.PI / 2) * 3)) {   //left
					x_goal = -1;
					y_goal = 0;
				}
			} 

        }

		if(moveTimer < 0) {
			y_goal = x_goal = 0;
		}else {
			moveTimer -= Time.deltaTime;
		}

		//set position
		partnerAnim.SetFloat("dance.y", y_goal, MOVE_SPEED, Time.deltaTime);
		partnerAnim.SetFloat("dance.x", x_goal, MOVE_SPEED, Time.deltaTime);

        //spawn arrows
        arrowTimer -= 1.0f*Time.deltaTime;
        if(arrowTimer <= 0) {
            
            if(Random.Range(0.0f, 1.0f) > (1.0f - arrowChance)) {
                placeArrow();
            }

            arrowTimer = arrowInterval;

        }

        //check successful presses
        if( Input.anyKeyDown ) { 
			float error = processKeyPresses();

            Color hitColor;
			if (error == -1.0f) {
				//complete miss
				slider.value += losePenalty/100.0f;
                hitColor = Color.yellow;
			} else {
				//got good
				slider.value -= (1-error)*winBonus/100.0f;
                hitColor = Color.green;
                slider.value -= cheatAmount;
                heartParticleSystem.Emit(hitEmitCount);

			}

            // funk!!
            // make the things glow
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            // left
            if (horizontal < -0.0f)
            {
                leftFader.currentColor = hitColor;
                Debug.Log("Left pressed");
            }
            // right
            else if (horizontal > 0.0f)
            {
                rightFader.currentColor = hitColor;
                Debug.Log("Right pressed");
            }

            // down
            if (vertical < -0.0f)
            {
                downFader.currentColor = hitColor;
                Debug.Log("Down pressed");
            }
            // up
            else if (vertical > 0.0f)
            {
                upFader.currentColor = hitColor;
                Debug.Log("Up pressed");
            }

        }


	}
    
    private float processKeyPresses() {
        float temp;
        temp = Mathf.Max(processKey("up", 0), processKey("down", 2), processKey("right", 3), processKey("left", 1));

		if(temp > 0) {
			temp = temp / winThresh;
		}

        return temp;
    }

    private float processKey(string keyString, float rot) {
        
        if (Input.GetKeyDown(keyString) || 
            (keyString == "up" && Input.GetAxis("Vertical") > 0.0f) ||
            (keyString == "down" && Input.GetAxis("Vertical") < 0.0f) ||
            (keyString == "left" && Input.GetAxis("Horizontal") < 0.0f) ||
            (keyString == "right" && Input.GetAxis("Horizontal") > 0.0f)

        ) {
            
            float err = -1.0f;

            for(int i=0; i<MAX_ARROWS; i++) {
                if(arrows[i].localRotation == Quaternion.EulerRotation(0,0,(Mathf.PI/2)*rot) && 
                    Mathf.Abs( arrows[i].localPosition.y - WIN_SPOT ) < winThresh ) {
                    err = Mathf.Abs( arrows[i].localPosition.y - WIN_SPOT );
                    arrows[i].gameObject.SetActive(false);
                }       
            }
            return err;
        }

        return -1.0f;

    }

    private void placeTemplateArrows() {
            
        upArrow = Instantiate( ArrowPrefab ) as RectTransform;
        upArrow.SetParent( this.transform );
        upArrow.localScale = Vector3.one*arrowScale;   

        downArrow = Instantiate( ArrowPrefab ) as RectTransform;
        downArrow.SetParent( this.transform );
        downArrow.localScale = Vector3.one*arrowScale;   

        rightArrow = Instantiate( ArrowPrefab ) as RectTransform;
        rightArrow.SetParent( this.transform );
        rightArrow.localScale = Vector3.one*arrowScale;   

        leftArrow = Instantiate( ArrowPrefab ) as RectTransform;
        leftArrow.SetParent( this.transform );
        leftArrow.localScale = Vector3.one*arrowScale;   

        upArrow.localRotation = Quaternion.EulerRotation(0,0,(Mathf.PI/2)*0);
        downArrow.localRotation = Quaternion.EulerRotation(0,0,(Mathf.PI/2)*2);
        rightArrow.localRotation = Quaternion.EulerRotation(0,0,(Mathf.PI/2)*3);
        leftArrow.localRotation = Quaternion.EulerRotation(0,0,(Mathf.PI/2)*1);

        upArrow.localPosition = new Vector3( (-1)*ARROW_X_SPREAD*(0.5f), WIN_SPOT);
        downArrow.localPosition = new Vector3( ARROW_X_SPREAD*(0.5f), WIN_SPOT);
        rightArrow.localPosition = new Vector3( ARROW_X_SPREAD*(1.5f), WIN_SPOT);
        leftArrow.localPosition = new Vector3( (-1)*ARROW_X_SPREAD*(1.5f), WIN_SPOT);

        upArrow.GetComponent<Image>().color = new Color(0.0f, 0.0f, 0.0f, 1f);
        downArrow.GetComponent<Image>().color = new Color(0.0f, 0.0f, 0.0f, 1f);
        leftArrow.GetComponent<Image>().color = new Color(0.0f, 0.0f, 0.0f, 1f);
        rightArrow.GetComponent<Image>().color = new Color(0.0f, 0.0f, 0.0f, 1f);

        upFader = upArrow.GetComponent<UIFader>();
        downFader = downArrow.GetComponent<UIFader>();
        rightFader = rightArrow.GetComponent<UIFader>();
        leftFader = leftArrow.GetComponent<UIFader>();

        Color templateColor = new Color(0.0f, 0.0f, 0.0f, 1f);
        upFader.targetColor = templateColor;
        downFader.targetColor = templateColor;
        leftFader.targetColor = templateColor;
        rightFader.targetColor = templateColor;
    }

    private void placeArrow() {

        // increment winbonus so you eventually win
        winBonus += 0.1f;

        int rot = Random.Range(0,4);
        arrows[current_arrow].localRotation = Quaternion.EulerRotation(0,0,(Mathf.PI/2)*rot);
        arrows[current_arrow].gameObject.SetActive(true);
        
        switch (rot) {
            case 0:   //up
                arrows[current_arrow].localPosition = new Vector3( (-1)*ARROW_X_SPREAD*(0.5f), TOP_SCREEN);
                break;
            
            case 2:   //down
                arrows[current_arrow].localPosition = new Vector3( ARROW_X_SPREAD*(0.5f), TOP_SCREEN);
                break;

            case 3:   //right
                arrows[current_arrow].localPosition = new Vector3( ARROW_X_SPREAD*(1.5f), TOP_SCREEN);
                break;
            
            case 1:   //left
                arrows[current_arrow].localPosition = new Vector3( (-1)*ARROW_X_SPREAD*(1.5f), TOP_SCREEN);
                break;
        }

        current_arrow = (current_arrow+1) % MAX_ARROWS;

    }

    public void SetCheat(float value)
    {
        cheatAmount += 0.1f;
    }
    
}
