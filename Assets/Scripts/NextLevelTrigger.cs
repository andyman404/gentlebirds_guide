﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class NextLevelTrigger : MonoBehaviour {
    public const int PLAYER_LAYER = 8;

    bool loaded = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.layer == PLAYER_LAYER && !loaded)
        {
            loaded = true;
            LoadNextScene();
        }
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
    }
}
