﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using UnityEngine.SceneManagement;


public class DanceDanceVictoryChecker : MonoBehaviour {

    public Slider slider;
    public bool won = false;

    public UnityEvent victoryEvent;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (slider.normalizedValue < 0.05f && !won)
        {
            won = true;
            victoryEvent.Invoke();
        }
	}

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
