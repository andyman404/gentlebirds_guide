﻿using UnityEngine;
using System.Collections;

public class DanceController : MonoBehaviour {

    private Animator anim;
    private float moveTimer;
    private float x_goal;
    private float y_goal;
    
    //constants
    private float MOVE_TIME = 18.0f/60.0f;
    private float MOVE_SPEED = 0.08f;
    
    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        anim.SetBool("dancing", true);
        moveTimer = MOVE_TIME;
	}
	
	// Update is called once per frame
	void Update () {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        //Input.GetAxis("Horizontal") < -0.1f 

//        if (Input.GetKeyDown("up")) {
        if (vertical > 0.1f) {
            moveTimer = MOVE_TIME;
            x_goal = 0;
            y_goal = 1;
        }

        if (vertical < -0.1f) {
//        if (Input.GetKeyDown("down")) {
            moveTimer = MOVE_TIME;
            x_goal = 0;
            y_goal = -1;
        }

        if (horizontal < -0.1f) {
//        if (Input.GetKeyDown("left")) {
            moveTimer = MOVE_TIME;
            x_goal = 1;
            y_goal = 0;
        }

        if (horizontal > 0.1f) {
//        if (Input.GetKeyDown("right")) {
            moveTimer = MOVE_TIME;
            x_goal = -1;
            y_goal = 0;
        }
        
        //set timer to limit dance time
        if(moveTimer < 0) {
            y_goal = x_goal = 0;
        }else {
            moveTimer-=Time.deltaTime;
        }

        //set position
        anim.SetFloat("dance.y", y_goal, MOVE_SPEED, Time.deltaTime);
        anim.SetFloat("dance.x", x_goal, MOVE_SPEED, Time.deltaTime);

    }

    private bool isAtMove(float threshold) {

        if (Mathf.Abs(anim.GetFloat("dance.x")) > threshold ||
            Mathf.Abs(anim.GetFloat("dance.y")) > threshold) {
            return true;
        } else {
            return false;
        }

    }

}
