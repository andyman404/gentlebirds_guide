﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
// interface for the bird's appearance
public class BirdAppearance : MonoBehaviour {
    public const int BODY_BLENDSHAPE_OBESE = 0;
    public const int BODY_BLENDSHAPE_SKINNY = 1;
    public const int BODY_BLENDSHAPE_FEMALE = 2;
    public const int HAIR_BLENDSHAPE_LENGTH = 0;
    public const int HAIR_BLENDSHAPE_STYLE1 = 1;
    public const int HAIR_BLENDSHAPE_STYLE2 = 2;

    public Color featherColor = Color.white;
    public Color hairColor = Color.white;

    public bool hasBowtie;
    public int hairTypeIndex = -1;

    [Range(0.0f, 1.0f)]
    public float hairLength = 0.0f; // 0.0 = short, 0.5 = medium, 1.0 = long

    [Range(0.0f, 1.0f)]
    public float hairOption = 0.5f; // 0.0 = style1, 0.5 = medium, 1.0 = style2

    [Range(0.0f, 1.0f)]
    public float gender = 0.0f; // 0.0 = male, 1.0 = female

    [Range(0.0f, 1.0f)]
    public float fatness = 0.5f; // 0.0 = skinny, 0.5 = normal, 1.0 = fat

    public SkinnedMeshRenderer bodyRenderer = null;
    public SkinnedMeshRenderer bowtieRenderer = null;
    public SkinnedMeshRenderer[] hairTypeRenderers = null;

    public bool shouldRandomizeGender = false;
    public bool shouldRandomizeAppearance = true;
    public bool backgroundDancer = false;

    private Material bodyMaterial = null;

	// Use this for initialization
	void Start () {
        bodyMaterial = bodyRenderer.materials[0];
        if (shouldRandomizeGender)
        {
            gender = (Random.value > 0.5f) ? Random.Range(0.75f, 1.0f) : Random.Range(0.0f, 0.25f);
        }
        if (shouldRandomizeAppearance)
        {
            PickRandomHairType();
            featherColor = (new HSBColor(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 0.5f), Random.Range(0.5f, 1.0f))).ToColor();
            hairColor = (new HSBColor(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.5f, 1.0f))).ToColor();
            fatness = (Random.value + Random.value + Random.value) / 3.0f;
            hairLength = Random.value;
            hasBowtie = false;
            transform.localScale = new Vector3(Random.Range(0.95f, 1.05f), Random.Range(0.95f, 1.05f), Random.Range(0.95f, 1.05f));
        }
        RefreshAppearance();

        if (backgroundDancer)
        {
            GetComponent<Animator>().SetBool("background_dancing", true);
        }
	}

    // just a scratch list so that there is garbage collection if this is called a lot
    private static List<int> tempHairList = new List<int>();

    // pick a random hair type for this gender
    void PickRandomHairType()
    {

        tempHairList.Clear();

        for (int i = 0; i < hairTypeRenderers.Length; i++)
        {
            HairData hairData = hairTypeRenderers[i].GetComponent<HairData>();
            if (hairData != null
                && (gender < 0.5f && hairData.forMale
                    || gender >= 0.5f && hairData.forFemale)
            ){
                tempHairList.Add(i);
            }
        }

        if (tempHairList.Count > 0)
        {
            hairTypeIndex = tempHairList[Random.Range(0, tempHairList.Count)];
        }
    }
	// Update is called once per frame
	void Update () {
        RefreshAppearance();
	}

    void RefreshAppearance()
    {
        // body
        bodyMaterial.color = featherColor;

        // gender
        bodyRenderer.SetBlendShapeWeight(BODY_BLENDSHAPE_FEMALE, gender * 100.0f);

        // obesity
        bodyRenderer.SetBlendShapeWeight(BODY_BLENDSHAPE_OBESE, Mathf.Max(0.0f, fatness - 0.5f) * 200.0f);
        bodyRenderer.SetBlendShapeWeight(BODY_BLENDSHAPE_SKINNY, Mathf.Min(0.5f, 0.5f-fatness) * 200.0f);

        // tie
        bowtieRenderer.gameObject.SetActive(hasBowtie);

        // hair - disable all the hair renderers except for the selected one 
        for (int i = 0; i < hairTypeRenderers.Length; i++)
        {
            bool hairActive = (i == hairTypeIndex);

            hairTypeRenderers[i].gameObject.SetActive(hairActive);

            // if the hair type is active, then set the blendshape and color params
            if (hairActive)
            {
                hairTypeRenderers[i].material.color = hairColor;
                int hairBlendShapeCount = hairTypeRenderers[i].sharedMesh.blendShapeCount;
                if (hairBlendShapeCount > 0)
                {
                    // hair length
                    hairTypeRenderers[i].SetBlendShapeWeight(HAIR_BLENDSHAPE_LENGTH, hairLength * 100.0f);

                    // hair style option
                    if (hairTypeRenderers[i].sharedMesh.blendShapeCount > 1)
                    {
                        
                        hairTypeRenderers[i].SetBlendShapeWeight(HAIR_BLENDSHAPE_STYLE2, Mathf.Max(0.0f, hairOption - 0.5f) * 200.0f);
                        hairTypeRenderers[i].SetBlendShapeWeight(HAIR_BLENDSHAPE_STYLE1, Mathf.Min(0.5f, 0.5f - hairOption) * 200.0f);
                    }
                }
            }
        }
    }
    public void SetBackgroundDancer(bool value)
    {
        backgroundDancer = value;
        if (backgroundDancer)
        {
            GetComponent<Animator>().SetBool("background_dancing", value);
            GetComponent<Animator>().SetBool("dancing", !value);
        }
    }
}
