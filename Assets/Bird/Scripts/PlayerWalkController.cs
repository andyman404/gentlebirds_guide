﻿using UnityEngine;
using System.Collections;

public class PlayerWalkController : MonoBehaviour {

    public float speed = 4.0f;

    private Animator anim;
    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	    
        // inputs
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 v = (new Vector3(horizontal, 0.0f, vertical)).normalized;
       
        v *= speed;

        rb.velocity = v;
        float currentSpeed = v.magnitude;
        if (currentSpeed > 0.0f)
        {
            rb.MoveRotation(Quaternion.LookRotation(v));
        }

        // animations
        anim.SetFloat("speed", currentSpeed);
	}
        
}
