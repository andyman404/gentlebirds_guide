﻿using UnityEngine;
using System.Collections;

// saves the player appearance to these static variables.
// on load, loads the player appearance from the static variables.
public class PlayerAppearanceSaver : MonoBehaviour {
    public static Color featherColor = Color.white;
    public static Color hairColor = Color.white;
    public static float hairLength = 0.5f;
    public static float hairOption = 0.5f;

    BirdAppearance appearance;

	// Use this for initialization
	void Start () {
        appearance = GetComponent<BirdAppearance>();

        appearance.featherColor = featherColor;
        appearance.hairColor = hairColor;
        appearance.hairLength = hairLength;
        appearance.hairOption = hairOption;
	}
	
	// Update is called once per frame
	void Update () {
        featherColor = appearance.featherColor;
        hairColor = appearance.hairColor;
        hairLength = appearance.hairLength;
        hairOption = appearance.hairOption;
	}
}
