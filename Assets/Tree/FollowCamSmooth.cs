﻿using UnityEngine;
using System.Collections;
using System.Net;
using UnityEngine.Events;

[System.Serializable]
public class CamWaypoint {
    public Transform wp;
    public float duration;
    public UnityEvent onStart;

}

public enum SmoothingType {
    smoothDamp = 0,
    smoothStep = 1,
    linear = 2
}

public class FollowCamSmooth : MonoBehaviour {
    public SmoothingType smoothingType = SmoothingType.smoothDamp;
    public CamWaypoint[] waypoints;
    public int nextWaypointIndex = 0;

    float startTime;
    float endTime = -1.0f;
    Vector3 velocity = Vector3.zero;

    Vector3 destination;
    Vector3 startPosition;
    Quaternion targetRotation;
    Quaternion startRotation;
	// Use this for initialization
	void Start () {
        transform.position = waypoints[0].wp.position;
        transform.rotation = waypoints[0].wp.rotation;
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time > endTime && nextWaypointIndex < waypoints.Length)
        {
            startTime = Time.time;
            endTime = Time.time + waypoints[nextWaypointIndex].duration;
            startPosition = transform.position;
            destination = waypoints[nextWaypointIndex].wp.position;
            targetRotation = waypoints[nextWaypointIndex].wp.rotation;
            startRotation = transform.rotation;
            waypoints[nextWaypointIndex].onStart.Invoke();
            nextWaypointIndex++;
        }
        if (smoothingType == SmoothingType.smoothDamp)
        {
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, endTime - Time.time);
        }
        else if (smoothingType == SmoothingType.smoothStep)
        {
            transform.position = Vector3.Lerp(startPosition, destination, Mathf.SmoothStep(0.0f, 1.0f, Mathf.InverseLerp(startTime, endTime, Time.time)));
        }
        else
        {
            transform.position = Vector3.Lerp(startPosition, destination, Mathf.InverseLerp(startTime, endTime, Time.time));
        }
//        transform.rotation = Quaternion.Slerp(startRotation, targetRotation, Mathf.SmoothStep(0.0f, 1.0f, Mathf.InverseLerp(startTime, endTime, Time.time)));
        transform.rotation = Quaternion.Slerp(startRotation, targetRotation, Mathf.InverseLerp(startTime, endTime, Time.time));

    }
}
